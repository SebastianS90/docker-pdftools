FROM debian:buster
MAINTAINER Sebastian Schloßer <sebastian@schlosser-nw.de>

RUN apt-get update -qq -y \
 && apt-get install pdftk texlive-extra-utils default-jre-headless wget -y \
 && wget -q -O sejda-console.deb https://github.com/torakiki/sejda/releases/download/v3.2.85/sejda-console_3.2.85-1_all.deb \
 && dpkg -i sejda-console.deb \
 && rm sejda-console.deb \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
