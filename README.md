# Docker Image for PDF Manipulation

**Image Location**: `registry.gitlab.com/sebastians90/docker-pdftools:latest` (built by [GiltLab CI](https://gitlab.com/SebastianS90/docker-pdftools/pipelines))

This Docker image is based on [Debian](https://hub.docker.com/_/debian/) and comes with the following packages installed:

- [`pdftk`](https://packages.debian.org/stable/pdftk) (tool for manipulating PDF documents)
- [`texlive-extra-utils`](https://packages.debian.org/stable/texlive-extra-utils) (TeX auxiliary programs)
  - `pdfnup`, `pdfjoin`, ...
- [`sejda-console`](https://github.com/torakiki/sejda) (An extendible and configurable PDF manipulation layer library written in java)
  - e.g. `sejda-console splitbybookmarks`

## License
This Dockerfile and the CI configuration is released into the public domain. Please see the [LICENSE](LICENSE) file for details.
For the license of software that is downloaded by this Dockerfile, please refer to their appropriate vendors.
